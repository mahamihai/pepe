#include <stdio.h> /* printf, scanf, NULL */
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <gmp.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#define BILLION 1E9
#define noop
bool isPrimeLarge(const mpz_t number)
{
    //    printf("Checked is :");
    //                 mpz_out_str(stdout,10,number);
    //     printf("\n");

     mpz_t middle;
    mpz_init_set(middle,number);
   

    mpz_div_ui(middle,middle,2);

     mpz_add_ui(middle,middle,1);


      mpz_t aux;
    mpz_init(aux);
    mpz_set_ui(aux,2);
      mpz_t remainder;
    mpz_init(remainder);
    mpz_set_ui(remainder,0);
        //          printf("Middle is :");
        //             mpz_out_str(stdout,10,middle);
        // printf("\n");
//  printf("Aux is :");
//                     mpz_out_str(stdout,10,aux);
//         printf("\n");
     //   printf("%d",mpz_cmp(middle,aux));
    while(mpz_cmp(middle,aux)>=0)
    {
        // printf("Aux is :");
        //             mpz_out_str(stdout,10,aux);
        // printf("\n");

         mpz_mod(remainder,number,aux);
        // printf("Remainder is :");
        //             mpz_out_str(stdout,10,remainder);
        // printf("\n");

        if(mpz_cmp_ui(remainder,0)==0)
        {
                
              mpz_clear(remainder);
                    mpz_clear(middle);
                          mpz_clear(aux);
                          return false;
        }
   
        mpz_add_ui(aux,aux,1);
    }
    mpz_clear(remainder);
    mpz_clear(middle);
    mpz_clear(aux);
    return true;

}
bool computeLargerPrime(mpz_t numbero)
{
//     printf("Computin a larger prime for");
//      mpz_out_str(stdout,10,numbero);
//                 printf("\n");
    mpz_t index;
     mpz_init(index);

    mpz_set_ui(index,numbero);
    mpz_add_ui(index,numbero,2);
   // mpz_out_str(stdout,10,index);
    printf("\n");
        while(1)
        {
            if(isPrimeLarge(index))
            {
                printf("\nThis is the fortunate number");
              
                mpz_sub(index,index,numbero);
                mpz_out_str(stdout,10,index);
                printf("\n");
                mpz_clear(index);
                
                return true;
            }
              mpz_out_str(stdout,10,index);
            mpz_add_ui(index,index,1);
           mpz_out_str(stdout,10,index);
                    
        }
    
                return false;

}
bool isPrime(int nr)
{
    if(nr==2){
        return true;
    }
if(nr %2 ==0)
    {
        return false;
    }
    for (int i = 3; i <= nr / 2; i+=2)
    {
        if (nr % i == 0)
        {
            return false;
        }
    }
    return true;
}

bool *prime;
int nrThreads = -1;
int bound = -1;
int *fortunes;
void *checkPrim(void *args)
{
    int tid = *((int *)args);
    // printf("The tid is :%d\n",tid);

    for (int i = 2 + tid; i  < bound; i += nrThreads)
    {
        // printf("I am in %d and i=%d\n",tid,i);

     
                prime[i] = isPrime(i);            
        
    }

    pthread_exit(NULL);
}
int nrFortuneNumbers;
void *findFortune(void *args)
{
    int tid = *((int *)args);
    // printf("The fortune tid is :%d\n",tid);

    for (int i = 1 + tid; i <= nrFortuneNumbers; i += nrThreads-(nrThreads+1)%2)
    {

        unsigned long long sum = 1;
        mpz_t suma;
        mpz_init(suma);
        mpz_set_ui(suma,1);
        int c = i;
         int j = 2;
        while (c > 0 && j < bound)
        {
            if (prime[j])
            {
                sum *= j;
                unsigned long castat=j;
                mpz_mul_ui(suma,suma,j);
                c--;
            }
            j++;
        }
        // printf("i=%d sum=%llu\n",i,sum);
        if (c > 0)
        {
            printf("Need bigger primes");
        }
        else
        {
                computeLargerPrime(suma);
            
     
        }
    }
   
    pthread_exit(NULL);
}
void doTheCiur(int maxNr, int nrThreads, int nrFortunes)
{
    clock_t start = clock();

    bound = maxNr;
    prime = (bool *)calloc(bound, sizeof(bool));
    memset(prime, true, sizeof(bool) * bound);
    fortunes = (int *)calloc(bound, sizeof(int));

    pthread_t threads[nrThreads];
    for (int i = 0; i < nrThreads; i++)
    {
        int *tid = (int *)malloc(sizeof(int));
        *tid = i;
        pthread_t aux;
        pthread_create(&aux, NULL, checkPrim, tid);
        threads[i] = aux;
    }
   
        pthread_t fThreads[nrFortunes];

    for (int i = 0; i < nrThreads; i++)
    {
        pthread_t aux = threads[i];
        pthread_join(threads[i], NULL);
       
    }
    clock_t end = clock();

    float seconds = (float)(end - start) / CLOCKS_PER_SEC;
    seconds *= 1000;

   // printf(" The prime op took %f\n", seconds);
    start = clock();
    for (int i = 0; i < nrThreads; i++)
    {
        pthread_t aux;
        int *tid = (int *)malloc(sizeof(int));
        *tid = i;
        pthread_create(&aux, NULL, findFortune, tid);
        fThreads[i] = aux;
    }
  
    for (int i = 0; i < nrThreads; i++)
    {
        // printf("Waiting for %d\n",i);
        pthread_join(fThreads[i], NULL);
        //printf("Finished %d\n",i);
    }

     end = clock();

     seconds = (float)(end - start) / CLOCKS_PER_SEC;
    seconds *= 1000;
    //printf(" The fortune op took %f\n", seconds);
    
}
int main(int argc, char **argv)
{
    int nrs = atoi(argv[1]);
   int nrThreadsInput = atoi(argv[2]);
    nrFortuneNumbers = atoi(argv[3]);
    double mean = 0;
    double allSecs = 0;
    int nrLoops = 10;
    for(int tr=1;tr<=nrThreadsInput;tr++)
    {
        allSecs=0;
        nrThreads=tr;
  
        //printf("Now at %d\n", i);

        clock_t start = clock();
        struct timeval stop, start1;
         struct timespec requestStart, requestEnd;
            clock_gettime(CLOCK_MONOTONIC, &requestStart);
        gettimeofday(&start1, NULL);
//do stuff

        doTheCiur(nrs, nrThreads, nrFortuneNumbers);
       gettimeofday(&stop, NULL);
      //  printf("took %lu\n", stop.tv_usec - start1.tv_usec);
        clock_t end = clock();
         clock_gettime(CLOCK_MONOTONIC, &requestEnd);
          double accum = ( requestEnd.tv_sec - requestStart.tv_sec )
                        + ( requestEnd.tv_nsec - requestStart.tv_nsec )
                        / BILLION;
        double seconds = (double)(end - start) / CLOCKS_PER_SEC;
           double seconds1 = (stop.tv_usec - start1.tv_usec)/(double)1000;
        seconds *= 1000;
       // printf("\nThe total time is     %f\n", seconds);
        allSecs += seconds1;
         for (int i = 1; i < bound; i++)
        {
            // printf("%d ",fortunes[i]);
           // (fortunes[i] > 0) ? printf("->%d ", fortunes[i]) : printf("");
        }
    
    printf("I've waited %lf\n",accum);
    printf("Total time for %d threads is %lf\n", nrThreads, seconds );
    }
}