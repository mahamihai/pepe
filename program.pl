:-dynamic noPrime/1.
:-dynamic isFortune/1.
:-dynamic isPrime/1.
:-dynamic primesList/1.
:- debug.
isPrime(I,N):-
    Half is div(N,2),
    I=<Half,
    0 is mod(N,I), 
    !,
    fail.
isPrime(I,N):-
    Half is div(N,2),
    I=<Half,
    !,
    I1 is I+1,
    isPrime(I1,N).
isPrime(_,_).




create_workers(0).

create_workers(N,Bound,NrThreads):-
    N>0,!,
    Start is N+1,
    thread_create(computePrimes(N,Start,NrThreads,Bound),Id,[]),
    
    N1 is N-1,
    create_workers(N1,Bound,NrThreads),
    thread_join(Id).
create_workers(0,_,_).

create_fortune_workers(N):-
    N>0,!,
   
    thread_create(computeFortunes(N),PId,[]),
    
    N1 is N-1,
   
    create_fortune_workers(N1),
     thread_join(PId).
create_fortune_workers(0).
getProduct(_,[],_,_):-
    write("Not enough prime numbers~n").

getProduct(N,[R|L],M):-
    N>0,
    !,
    N1 is N-1,
    getProduct(N1,L,M1),
    M is R*M1.
getProduct(0,_,1).
getBigger(M,[R|_],R):-
    M1 is M+1,
    M1<R,
    !.
getBigger(M,[R|L],U):-
    M1 is M+1,
    M1>=R,
    !,
    getBigger(M,L,U).
getBigger(_,[],-1):-
    write("Cannot find a bigger number~n").
checkAndStoreFortune(Elem):-
    Elem>0,
    !,
      asserta(isFortune(Elem)).
checkAndStoreFortune(_).

computeFortunes(N):-
        primesList(R),
      getProduct(N,R,M),
      
     getBigger(M,R,Elem),
      FortuneElem is Elem-M,
   
      format("asserting a fortune number ~d ~d ~d ~n",[N,M,Elem]),
      checkAndStoreFortune(FortuneElem).

setPrimes(Bound):-
    Bound>1,
    not(noPrime(Bound)),
    !,
    asserta(isPrime(Bound)),
     Bound1 is Bound-1,
    setPrimes(Bound1).
setPrimes(Bound):-
    Bound>1,
    Bound1 is Bound-1,
    setPrimes(Bound1).
setPrimes(1).
    
checkPrime(Nr):-
     isPrime(2,Nr),
     !,
     format("Found ~d ~n",[Nr]),
      asserta(isPrime(Nr)).
checkPrime(_).

computePrimes(Id,Step,NrThreads,Bound):-
    Step =< Bound, 

    !,
  checkPrime(Step),
      Step1 is Step + NrThreads,
   computePrimes(Id,Step1,NrThreads,Bound).

computePrimes(_,_,_,_).

remove(Step,Bound,Index):-
    Step=<Bound,
    !,

    asserta(noPrime(Step)),
    NextStep is Step+Index,
    remove(NextStep, Bound,Index).
remove(_,_,_).


start(Bound,Fortunes,NrThreads,R,F):-
    retractall(noPrime(P1)),
     retractall(isPrime(P2)),
      retractall(isFortune(P3)),
          retractall(primesList(P4)),
        write("Creating thread"),
    thread_create(create_workers(NrThreads,Bound,NrThreads),PrimeId,[]),

    thread_join(PrimeId),
 
     findall(A,isPrime(A),L),
     sort(L,R),
          write("DOne with primes"),

       % write(R),
     retractall(isPrime(_)),
     asserta(primesList(R)),
      
     thread_create(create_fortune_workers(Fortunes),FId,[]),
      thread_join(FId),
      findall(X,isFortune(X),F),
     write(F).
    

yuhu(Numbers,NrF,R,F,Threads):-
    
    retractall(isFortune(_)),
    retractall(noPrime(_)),
    retractall(isPrime(_)),
    write("Starting"),
    start(Numbers,NrF,Threads,R,F).

     
runStats(Numbers,NrF,NrThreads):-

    
     statistics(walltime,[Start|_]),
    
    yuhu(Numbers,NrF,R,F,NrThreads),
         

    statistics(walltime,[Stop|_]),
    TimeF is Stop-Start,
    write(TimeF).



   
    







runIterations(NrThreads,FortuneNumbers,NrIters):-
    NrThreads>0,
    !,
    runStats(FortuneNumbers,NrThreads,NrIters,0,Time),
    Average is div(Time,NrIters),
    format('With a numbers of ~d threads is have a time ~3d ~n',[NrThreads,Time]),
    NewNrThreads is NrThreads - 1,
    runIterations(NewNrThreads,FortuneNumbers,NrIters).
runIterations(0,_,_):-
    abolish(isFortune(_)),
    abolish(noPrime(_)),
    abolish(isPrime(_)).
    
