import java.math.BigDecimal;
import static java.lang.Math.toIntExact;
import java.math.BigInteger;

class teste{
    boolean isPrime(BigInteger test)
    {
        BigInteger middle=new BigInteger(test.toString());
        middle=middle.divide(new BigInteger("2"));
        middle=middle.add(new BigInteger("1"));
        BigInteger divisor=new BigInteger("2");
        BigInteger zero=new BigInteger("0");
        while(divisor.compareTo(middle)<0)
        {
            if(test.mod(divisor).compareTo(zero)==0)
            {
                return false;
            }
            divisor=divisor.add(new BigInteger("1"));
        }
        return true;

    }
    BigInteger getBiggerPrime(BigInteger start)
    {
        BigInteger index=new BigInteger(start.toString());
        index=index.add(BigInteger.valueOf(2));
       
        while(true)
        {
            if(isPrime(index))
            {
                System.out.println("Found a fortune number as "+index.subtract(start).toString());
                return index;
            }
            index=index.add(new BigInteger("1"));
        }
       
    }
public static void main(String args[]) {
   teste t=new teste();
   var m=t.getBiggerPrime(new BigInteger("30"));
}
}