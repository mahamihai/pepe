import java.math.BigDecimal;
import static java.lang.Math.toIntExact;
import java.math.BigInteger;
public class Fortuna implements Runnable {
    public Fortuna(int nrFortunes, int nrThreads, int id, boolean[] primes,int bound) {
        super();
        this.nrFortunes = nrFortunes;
        this.id = id;
        this.primes = primes;
        this.nrThreads = nrThreads;
        this.bound=bound;
    }

    int nrFortunes;
    int id;
    boolean[] primes;
    int nrThreads;
    int bound;
    boolean isPrime(BigInteger test)
    {
        BigInteger middle=new BigInteger(test.toString());
        middle=middle.divide(new BigInteger("2"));
        middle=middle.add(new BigInteger("1"));
        BigInteger divisor=new BigInteger("2");
        BigInteger zero=new BigInteger("0");
        while(divisor.compareTo(middle)<0)
        {
            if(test.mod(divisor).compareTo(zero)==0)
            {
                return false;
            }
            divisor=divisor.add(new BigInteger("1"));
        }
        return true;

    }
    BigInteger getBiggerPrime(BigInteger start)
    {
      //  System.out.println("Finding for "+start.toString());
        BigInteger index=new BigInteger(start.toString());
        index=index.add(BigInteger.valueOf(2));
       
        while(true)
        {
            if(isPrime(index))
            {
                System.out.println("Found a fortune number as "+index.subtract(start).toString());
                return index;
            }
            index=index.add(new BigInteger("1"));
        }
       
    }
    public void run() {
        for (int i = 1 + id; i <= nrFortunes; i += nrThreads) {
            int sum=1; 
            int c = i;
            int j = 2;
            BigInteger suma=new BigInteger("1");
            while (c > 0 && j < bound) {
                if (primes[j]) {
                    sum*=j;
                    //System.out.println("Multiplying with "+j);
                    suma=suma.multiply(BigInteger.valueOf(j));
                    c--;
                }
                j++;
            }
           // System.out.println(i+" "+ sum);
            if (c > 0) {
               // System.out.println("Need bigger primes");

            } else {
                
                getBiggerPrime(suma);
            }
        }

    }
}