import java.util.Arrays;

class Program {

    public static void main(String[] args) {
        int bound = Integer.parseInt(args[0]);
        int nrThreads = Integer.parseInt(args[1]);
        int nrFortunes = Integer.parseInt(args[2]);
        System.out.println("x");
  
       
        for(int l=1;l<=8;l++)
        {
            long allTime=0;
                 nrThreads=l;
    
            Thread[] threads = new Thread[nrThreads];
            boolean primes[] = new boolean[bound];
            Arrays.fill(primes, true);
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < nrThreads; i++) {
                threads[i] = new Thread(new Erastotenes(i, nrThreads, bound, primes));
                threads[i].start();
            }
            for (int i = 0; i < nrThreads; i++) {
                try {
                    threads[i].join();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
           // System.out.print("DOne ");
            for (int i = 0; i < bound; i++) {
                // System.out.print((primes[i]?i+" ":""));
            }
            Thread[] fThreads = new Thread[nrThreads];
            for (int i = 0; i < nrThreads; i++) {
                threads[i] = new Thread(new Fortuna(nrFortunes, nrThreads, i, primes, bound));
                threads[i].start();
            }
            for (int i = 0; i < nrThreads; i++) {
                try {
                    threads[i].join();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;
           
        
        System.out.println("The time is :   " + elapsedTime+" for nr of threads "+nrThreads);
    }

       
    }
}