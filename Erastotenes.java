public class Erastotenes implements Runnable
{
    private int id;
    private int bound;
    private int nrThreads;
    private boolean[] prime;
    public Erastotenes(int it,int nrThreads,int bound,boolean[] prime) {
        this.id=it;
        this.bound=bound;
        this.prime=prime;
        this.nrThreads=nrThreads;

    }
    public void run()
    {
        for(int i=2+id;i<bound;i+=this.nrThreads)
        {
            if(!Utils.isPrime(i))
            {
               
                prime[i]=false;

                
            }
        }
    }
}