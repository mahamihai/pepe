#include <gmp.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>

bool isPrime(const mpz_t number)
{
    //    printf("Checked is :");
    //                 mpz_out_str(stdout,10,number);
    //     printf("\n");

     mpz_t middle;
    mpz_init_set(middle,number);
   

    mpz_div_ui(middle,middle,2);

     mpz_add_ui(middle,middle,1);


      mpz_t aux;
    mpz_init(aux);
    mpz_set_ui(aux,2);
      mpz_t remainder;
    mpz_init(remainder);
    mpz_set_ui(remainder,0);
        //          printf("Middle is :");
        //             mpz_out_str(stdout,10,middle);
        // printf("\n");
//  printf("Aux is :");
//                     mpz_out_str(stdout,10,aux);
//         printf("\n");
     //   printf("%d",mpz_cmp(middle,aux));
    while(mpz_cmp(middle,aux)>=0)
    {
        // printf("Aux is :");
        //             mpz_out_str(stdout,10,aux);
        // printf("\n");

         mpz_mod(remainder,number,aux);
        // printf("Remainder is :");
        //             mpz_out_str(stdout,10,remainder);
        // printf("\n");

        if(mpz_cmp_ui(remainder,0)==0)
        {
                
              mpz_clear(remainder);
                    mpz_clear(middle);
                          mpz_clear(aux);
                          return false;
        }
   
        mpz_add_ui(aux,aux,1);
    }
    mpz_clear(remainder);
    mpz_clear(middle);
    mpz_clear(aux);
    return true;

}
bool computeLargerPrime(mpz_t numbero)
{
    mpz_t index;
     mpz_init(index);

    mpz_set_ui(index,numbero);
    mpz_add_ui(index,numbero,2);
    mpz_out_str(stdout,10,index);
    printf("\n");
        while(1)
        {
            if(isPrime(index))
            {
                // printf("This is the next prime");
                // mpz_out_str(stdout,10,index);
                // printf("\n");
                mpz_clear(index);
                return true;
            }
              mpz_out_str(stdout,10,index);
            mpz_add_ui(index,index,1);
           mpz_out_str(stdout,10,index);
                    
        }
    
                return false;

}
void getPrime()
{
   mpz_t test;
   mpz_init(test);
   mpz_set_ui(test,7);
    computeLargerPrime(test);

}
int main()

{
getPrime();
  return 0;

}