#include <stdio.h>
#include <stdio.h> /* printf, scanf, NULL */
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdio.h> /* printf, scanf, NULL */
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <sys/sysinfo.h>
#include <gmp.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#define BILLION 1E9

bool isPrimeLarge(const mpz_t number)
{
    //    printf("Checked is :");
    //                 mpz_out_str(stdout,10,number);
    //     printf("\n");

     mpz_t middle;
    mpz_init_set(middle,number);
   

    mpz_div_ui(middle,middle,2);

     mpz_add_ui(middle,middle,1);


      mpz_t aux;
    mpz_init(aux);
    mpz_set_ui(aux,2);
      mpz_t remainder;
    mpz_init(remainder);
    mpz_set_ui(remainder,0);
        //          printf("Middle is :");
        //             mpz_out_str(stdout,10,middle);
        // printf("\n");
//  printf("Aux is :");
//                     mpz_out_str(stdout,10,aux);
//         printf("\n");
     //   printf("%d",mpz_cmp(middle,aux));
    while(mpz_cmp(middle,aux)>=0)
    {
        // printf("Aux is :");
        //             mpz_out_str(stdout,10,aux);
        // printf("\n");

         mpz_mod(remainder,number,aux);
        // printf("Remainder is :");
        //             mpz_out_str(stdout,10,remainder);
        // printf("\n");

        if(mpz_cmp_ui(remainder,0)==0)
        {
                
              mpz_clear(remainder);
                    mpz_clear(middle);
                          mpz_clear(aux);
                          return false;
        }
   
        mpz_add_ui(aux,aux,1);
    }
    mpz_clear(remainder);
    mpz_clear(middle);
    mpz_clear(aux);
    return true;

}
bool computeLargerPrime(mpz_t numbero)
{
//     printf("Computin a larger prime for");
//      mpz_out_str(stdout,10,numbero);
//                 printf("\n");
    mpz_t index;
     mpz_init(index);

    mpz_set_ui(index,numbero);
    mpz_add_ui(index,numbero,2);
   // mpz_out_str(stdout,10,index);
    printf("\n");
        while(1)
        {
            if(isPrimeLarge(index))
            {
                printf("\nThis is the fortunate number");
              
                mpz_sub(index,index,numbero);
                mpz_out_str(stdout,10,index);
                printf("\n");
                mpz_clear(index);
                
                return true;
            }
              mpz_out_str(stdout,10,index);
            mpz_add_ui(index,index,1);
           mpz_out_str(stdout,10,index);
                    
        }
    
                return false;

}
unsigned long long *fortunes;
bool *primes;
int numbers;
int nrThreads;
int nrFortuneNumbers;

bool isPrime(int nr)
{
    if (nr == 2)
    {
        return true;
    }

    for (int i = 2; i <= nr / 2; i += 2)
    {
        if (nr % i == 0)
        {
            return false;
        }
    }
    return true;
}

void init(int numbers)
{
    free(primes);
    free(fortunes);
    primes = (bool *)calloc(numbers, sizeof(bool));
    fortunes = (unsigned long long*)calloc(nrFortuneNumbers+1, sizeof(unsigned long long));
}

void findFortune(int numThreads)
{
#pragma omp parallel for num_threads(numThreads)

    for (int i = 1; i <= nrFortuneNumbers; i++)
    {

        unsigned long long sum = 1;
        mpz_t suma;
        mpz_init(suma);
        mpz_set_ui(suma,1);
        int c = i;
        int j = 2;
        while (c > 0 && j < numbers)
        {
            if (primes[j])
            {
                sum *= j;
                  unsigned long castat=j;
                mpz_mul_ui(suma,suma,j);
                c--;
            }
            j++;
        }
        // printf("i=%d sum=%llu\n",i,sum);
        if (c > 0)
        {
            printf("Need bigger primes");
        }
        else
        {

           computeLargerPrime(suma);
        }
    }
}

void computePrimes(int numbers, int nrThreads)
{

#pragma omp parallel for num_threads(nrThreads)
    for (int i = 1; i < numbers; i++)
    {
        primes[i] = isPrime(i);
    }
}
int main(int ac, char **av)

{
    numbers = atoi(av[1]);

    nrFortuneNumbers = atoi(av[2]);
    int nrThreadsInput = atoi(av[3]);
    int nrIter = 100;

    for (int i = 1; i <= nrThreadsInput; i++)
    {
        /* code */
            double seconds=0;
        nrThreads=i;

   
        /* code */

        init(numbers);
        struct timeval start, end;
        struct timespec requestStart, requestEnd;
            clock_gettime(CLOCK_MONOTONIC, &requestStart);

        gettimeofday(&start, NULL);
        computePrimes(numbers, nrThreads);
       findFortune(nrThreads);
        for (int j = 1; j <= nrFortuneNumbers; j++)
        {
            if (fortunes[j]>0)
            {
               // printf("%llu ",fortunes[j]);
            }
        }
                    clock_gettime(CLOCK_MONOTONIC, &requestEnd);

        gettimeofday(&end, NULL);
        double accum = ( requestEnd.tv_sec - requestStart.tv_sec )
                        + ( requestEnd.tv_nsec - requestStart.tv_nsec )
                        / BILLION;
        unsigned long long int currentEx = 1000000ULL * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) ;
        printf("current %lf\n",accum);
        seconds+=currentEx;
        
    
        printf("Total time for %d threads is %lf\n\n", nrThreads, seconds / (double)1000000);
    }
    return 0;
}
